#!/bin/bash

echo "System prepping now..."
sleep 2
rm -f /.first_boot_complete

echo "Deleting any non root users"
for user in `tail -n +30 /etc/passwd | awk -F: '{print $1}'`
do
    echo -e "\tDeleting $user"
    userdel -rf $user
done
sleep 1

echo "Removing ssh keys"
systemctl stop ssh
rm -f /etc/ssh/ssh_host_*key*
sleep 1

echo "Cleaning up old lease files"
rm -f /var/lib/dhcp/*.leases
sleep 1
echo "Done..."
