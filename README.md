# Ansible "bootstrap" files
Used to get a base config onto a host at boot time.

## Performance
    apt-get update ; apt-get -y dist-upgrade ; apt-get -y install software-properties-common git-core ; apt-add-repository -y ppa:ansible/ansible ; apt-get update ; apt-get -y install ansible ; ansible-pull -U https://git.projectchilli.com/pfarmer/ansible-bootstrap.git -i performance/hosts performance/local.yml